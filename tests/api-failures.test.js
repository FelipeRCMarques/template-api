// External libraries
const request = require("supertest");

// API module
const API = require("../src/api");

describe("The project failures", () => {
  let api, mockColl, mockDb, mongoClient, stanConn;
  const corsOptions = { origin: "*" };

  const secret = "SUPERSECRET";

  beforeEach(() => {
    mockColl = {
      insertOne: jest.fn(),
      findOne: jest.fn(),
      deleteOne: jest.fn(),
    };

    mockDb = { collection: () => mockColl };

    mongoClient = { db: () => mockDb };

    stanConn = { publish: jest.fn() };

    api = API(corsOptions, { mongoClient, stanConn, secret });
  });

  it("tries to create an user without name", async () => {
    const pw = "123456foo";
    const newUser = {
      //name: "Foo",
      email: "foo@example.com",
      password: pw,
      passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
    expect(response.body).toStrictEqual({ "error": "Request body had missing field name"});
    expect(stanConn.publish).not.toHaveBeenCalled();
    expect(mockColl.findOne).not.toHaveBeenCalled();
    expect(mockColl.insertOne).not.toHaveBeenCalled();
  });

  it("tries to create an user without email", async () => {
    const pw = "123456foo";
    const newUser = {
      name: "Foo",
      //email: "foo@example.com",
      password: pw,
      passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
    expect(response.body).toStrictEqual({ "error": "Request body had missing field email"});
    expect(stanConn.publish).not.toHaveBeenCalled();
    expect(mockColl.findOne).not.toHaveBeenCalled();
    expect(mockColl.insertOne).not.toHaveBeenCalled();
  });

  it("tries to create an user without password", async () => {
    const pw = "123456foo";
    const newUser = {
      name: "Foo",
      email: "foo@example.com",
      //password: pw,
      passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
    expect(response.body).toStrictEqual({ "error": "Request body had missing field password"});
    expect(stanConn.publish).not.toHaveBeenCalled();
    expect(mockColl.findOne).not.toHaveBeenCalled();
    expect(mockColl.insertOne).not.toHaveBeenCalled();
  });

  it("tries to create an user without passwordConfirmation", async () => {
    const pw = "123456foo";
    const newUser = {
      name: "Foo",
      email: "foo@example.com",
      password: pw,
      //passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
    expect(response.body).toStrictEqual({ "error": "Request body had missing field passwordConfirmation"});
    expect(stanConn.publish).not.toHaveBeenCalled();
    expect(mockColl.findOne).not.toHaveBeenCalled();
    expect(mockColl.insertOne).not.toHaveBeenCalled();
  });

  it("tries to create an user with empty name", async () => {
    const pw = "123456foo";
    const newUser = {
      name: "",
      email: "foo@example.com",
      password: pw,
      passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
    expect(response.body).toStrictEqual({ "error": "Request body had malformed field name"});
    expect(stanConn.publish).not.toHaveBeenCalled();
    expect(mockColl.findOne).not.toHaveBeenCalled();
    expect(mockColl.insertOne).not.toHaveBeenCalled();
  });

  it("tries to create an user with empty email", async () => {
    const pw = "123456foo";
    const newUser = {
      name: "Foo",
      email: "",
      password: pw,
      passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
    expect(response.body).toStrictEqual({ "error": "Request body had malformed field email"});
    expect(stanConn.publish).not.toHaveBeenCalled();
    expect(mockColl.findOne).not.toHaveBeenCalled();
    expect(mockColl.insertOne).not.toHaveBeenCalled();
  });

  it("tries to create an user with invalid email", async () => {
    const pw = "123456foo";
    const newUser = {
      name: "Foo",
      email: "foo@examplecom",
      password: pw,
      passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
    expect(response.body).toStrictEqual({ "error": "Request body had malformed field email"});
    expect(stanConn.publish).not.toHaveBeenCalled();
    expect(mockColl.findOne).not.toHaveBeenCalled();
    expect(mockColl.insertOne).not.toHaveBeenCalled();
  });

  it("tries to create an user with empty password", async () => {
    const pw = "123456foo";
    const newUser = {
      name: "Foo",
      email: "foo@example.com",
      password: "",
      passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
    expect(response.body).toStrictEqual({ "error": "Request body had malformed field password"});
    expect(stanConn.publish).not.toHaveBeenCalled();
    expect(mockColl.findOne).not.toHaveBeenCalled();
    expect(mockColl.insertOne).not.toHaveBeenCalled();
  });

  it("tries to create an user with 2 characters password", async () => {
    const pw = "12";
    const newUser = {
      name: "Foo",
      email: "foo@example.com",
      password: pw,
      passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
    expect(response.body).toStrictEqual({ "error": "Request body had malformed field password"});
    expect(stanConn.publish).not.toHaveBeenCalled();
    expect(mockColl.findOne).not.toHaveBeenCalled();
    expect(mockColl.insertOne).not.toHaveBeenCalled();
  });

  it("tries to create an user with 33 characters password", async () => {
    const pw = "123456789012345678901234567890123";
    const newUser = {
      name: "Foo",
      email: "foo@example.com",
      password: pw,
      passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
    expect(response.body).toStrictEqual({ "error": "Request body had malformed field password"});
    expect(stanConn.publish).not.toHaveBeenCalled();
    expect(mockColl.findOne).not.toHaveBeenCalled();
    expect(mockColl.insertOne).not.toHaveBeenCalled();
  });

  it("tries to create an user with not alphanum character password", async () => {
    const pw = "123!";
    const newUser = {
      name: "Foo",
      email: "foo@example.com",
      password: pw,
      passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
    expect(response.body).toStrictEqual({ "error": "Request body had malformed field password"});
    expect(stanConn.publish).not.toHaveBeenCalled();
    expect(mockColl.findOne).not.toHaveBeenCalled();
    expect(mockColl.insertOne).not.toHaveBeenCalled();
  });

  it("tries to create an user with incorrect passwordConfirmation", async () => {
    const pw = "123456foo";
    const newUser = {
      name: "Foo",
      email: "foo@example.com",
      password: pw,
      passwordConfirmation: pw+"eae",
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(422);
    expect(response.body).toStrictEqual({ "error": "Password confirmation did not match"});
    expect(stanConn.publish).not.toHaveBeenCalled();
    expect(mockColl.findOne).not.toHaveBeenCalled();
    expect(mockColl.insertOne).not.toHaveBeenCalled();
  });

  it("tries to delete an user without Authentication header", async () => {
    const uid = "608ef5cc069020a1d61d5380";
    const correctToken =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwOGVmNWNjMDY5MDIwYTFkNjFkNTM4MCJ9.DU55f1y8dGSJPWYXrHUUwU0zGc-N8FixQqontudI4RE";

    const response = await request(api)
      .delete(`/users/${uid}`)
      .set("uthentication", `Bearer ${correctToken}`);

    expect(response.status).toBe(401);
    expect(response.body).toStrictEqual({ "error": "Access Token not found"});
    expect(mockColl.findOne).not.toHaveBeenCalled();
    expect(stanConn.publish).not.toHaveBeenCalled();
    expect(mockColl.deleteOne).not.toHaveBeenCalled();
  });

  it("tries to delete an user without Bearer in Authentication header", async () => {
    const uid = "608ef5cc069020a1d61d5380";
    const correctToken =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwOGVmNWNjMDY5MDIwYTFkNjFkNTM4MCJ9.DU55f1y8dGSJPWYXrHUUwU0zGc-N8FixQqontudI4RE";

    const response = await request(api)
      .delete(`/users/${uid}`)
      .set("uthentication", `earer ${correctToken}`);

    expect(response.status).toBe(401);
    expect(response.body).toStrictEqual({ "error": "Access Token not found"});
    expect(mockColl.findOne).not.toHaveBeenCalled();
    expect(stanConn.publish).not.toHaveBeenCalled();
    expect(mockColl.deleteOne).not.toHaveBeenCalled();
  });
});
