const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");
const Joi = require("joi")

const schema = Joi.object({
  name: Joi.string().required().messages({
    'any.required': "Request body had missing field name",
    'string.empty': "Request body had malformed field name"
  }),
  email: Joi.string().email().required().messages({
    'any.required': "Request body had missing field email",
    'string.empty': "Request body had malformed field email",
    'string.email': "Request body had malformed field email"
  }),
  password: Joi.string().alphanum().min(3).max(32).required().messages({
    'any.required': "Request body had missing field password",
    'string.empty': "Request body had malformed field password",
    'string.min': "Request body had malformed field password",
    'string.max': "Request body had malformed field password",
    'string.alphanum': "Request body had malformed field password"
  }),
  passwordConfirmation: Joi.ref('password')
}).with('password', 'passwordConfirmation')
  .messages({
    'object.with': "Request body had missing field passwordConfirmation",
    'any.only': "Password confirmation did not match"
  })

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  /* ******************* */
  /* YOUR CODE GOES HERE */
  /* ******************* */

  api.post("/users", (req, res) => {
    const { error, value } = schema.validate(req.body);

    if (error) {
      if (error.details[0].type === 'any.only')
        res.status(422).send({ error: error.details[0].message })

      if (error.details[0].type === 'any.required' || 'object.with' || 'string.empty' || 'string.min' || 'string.max' || 'string.alphanum' || 'string.email')
        res.status(400).send({ error: error.details[0].message })
    }

    const uid = uuid()

    const user = {
      user:
      {
        id: uid,
        name: value.name,
        email: value.email
      }
    }

    res.status(201).send(user)

    stanConn.publish('users', {
      "eventType": "UserCreated",
      "entityId": uid,
      "entityAggregate": {
        "name": value.name,
        "email": value.email,
        "password": value.password
      }
    })

    mongoClient.db().collection().findOne();

  })

  api.delete("/users/:uuid", (req, res) => {
    const authHeader = req.headers.authentication

    const [schema, bearerToken] = authHeader ? authHeader.split(' ') : []

    if ((!schema || schema.toLowerCase() !== 'bearer') || !bearerToken)
      res.status(401).send({ error: "Access Token not found", })

    const payload = jwt.verify(bearerToken, secret)
    const uid = req.params.uuid

    if (payload.id != uid)
      res.status(403).send({ error: "Access Token did not match User ID", })

    res.status(200).send({id: uid})

    mongoClient.db().collection().findOne(uid);

    stanConn.publish('users', {
      "eventType": "UserDeleted",
      "entityId": uid,
      "entityAggregate": {}
    })

  })

  return api;
};